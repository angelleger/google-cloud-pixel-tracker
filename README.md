# Google Cloud Pixel Tracker

A pixel tracker for saving into Google Datastore entries from a website and updating the results into a Google Spreesheet with the Google API

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need:

Nodejs installed  
Credentials from Google Cloud Platform 


### Installing


1 - You need to access to you google cloud console then, copy the repo into the folder you are going to deploy
```
$ git clone https://angelleger@bitbucket.org/angelleger/gcloud-pixel-tracker.git FOLDER  
```
2 - Install dependencies  
```
npm install  
```
3 - Copy the credentials fro the API's  
```
credentials.json  
```
```
client_secret.json  
```
4 - Run the app
```
npm start  
```

Now you can access to the app with the web preview in the google console.

## Running the tests

Pending

### Break down into end to end tests

Pending

## Deployment

Pending

## Built With

* [nodejs-datastore](https://github.com/googleapis/nodejs-datastore/) - Node.js client for Google Cloud Datastore: a highly-scalable NoSQL database for your web and mobile applications.
* [node-cron](https://github.com/kelektiv/node-cron) - Cron for NodeJS.
* [googleapis](https://github.com/googleapis/googleapis) - Public interface definitions of Google APIs.
* [moment](https://github.com/moment/moment) - Parse, validate, manipulate, and display dates in javascript.

## Versioning

1.0.0

## Authors

* **Angel Leger** - *webpagefx* - [angelleger](https://bitbucket.org/angelleger/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Pending

