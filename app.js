'use strict';

const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');
const moment = require('moment');
const OAuth2Client = google.auth.OAuth2;
const http = require('http');
const tracker = require('pixel-tracker');
var CronJob = require('cron').CronJob;
var crypto = require('crypto');
const spreadsheetId = "1PsRwvZRLf_ZsxiLeMpxcuxlX3WC4lIJjtTvruEG5tDo";
var queryCounts = 0;
var readCounts = 0;


// Imports the Google Cloud client library
const Datastore = require('@google-cloud/datastore');
// Google Cloud Platform project ID
const projectId = 'influencer-web-beacon';
const yesterday = moment().subtract(1, "days").format("DD-MM-YYYY").replace(new RegExp("/", "g"), '-');

// Creates a client
const datastore = new Datastore({
    projectId: projectId,
});

const SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];
const TOKEN_PATH = 'credentials.json';

tracker
    .use(function(error, result) {
        // Saving the visit
        saveEntry(result);
  
    })
    .configure({
        disable_cookies: true
    });



//at 3am running cron once a day 00 00 03 * * 0-6
// for testong it's every every 59 seconds
var job = new CronJob('0 */3 * * * *', function() {
    queryCounts  = 0;
        fs.readFile('client_secret.json', (err, content) => {
            if (err) return console.log('Error loading client secret file:', err);

            authorize(JSON.parse(content), listUrls);
         
            console.log("Updating spreadsheet: "+ moment().format('MMMM Do YYYY, h:mm:ss a')); 
        });

    }, function() {

    },
    true, /* Start the job right now */
    'America/Los_Angeles',
);


/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {

    const {
        client_secret,
        client_id,
        redirect_uris
    } = credentials.installed;
    const oAuth2Client = new OAuth2Client(client_id, client_secret, redirect_uris[0]);

    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, (err, token) => {
        if (err) return getNewToken(oAuth2Client, callback);
        oAuth2Client.setCredentials(JSON.parse(token));
        callback(oAuth2Client);
    });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {

    const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES,
    });
    console.log('Authorize this app by visiting this url:', authUrl);
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });
    rl.question('Enter the code from that page here: ', (code) => {
        rl.close();
        oAuth2Client.getToken(code, (err, token) => {
            if (err) return callback(err);
            oAuth2Client.setCredentials(token);
            // Store the token to disk for later program executions
            fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
                if (err) console.error(err);
                console.log('Token stored to', TOKEN_PATH);
            });
            callback(oAuth2Client);
        });
    });
}

/**
 * Someone call the Beacon so let's save the visit on the DB
 * @param {Object} result the object with teh information of the visit
 */
function saveEntry(result) {

    // The kind for the new entity
    const kind = 'visits';

    // The Cloud Datastore key for the new entity
    const visitKey = datastore.key([kind]);

    // Prepares the new entity
    const visit = {
        key: visitKey,
        data: {
            _uniqueId: crypto.createHash('md5').update(result.referer).digest("hex"),
            date: moment().format("DD-MM-YYYY"),
            date_time: moment().format('LT'),
            referer_url: result.referer,
            browser: result.useragent.browser,
            browser_version: result.useragent.version,
            browser_lang: result.language[1],
            host: result.host,
            domain: result.domain,
        },
    };

    // Saves the entity
    datastore
        .save(visit)
        .then(() => {
            console.log(`Saved  ${visit.data.date}`);
        })
        .catch(err => {
            console.error('ERROR:', err);
        });
}

/**
 * let's get the total traffic count by URL
 * @param {String} url The published URL.
 * @param {Number} i The published URL row number in the array spreedsheet
 * @param {Boolean} total To see if is the total in general or total from yesterday
 * @param {function} callback The callback to call.
 */
function getUrlTrafficCount(url, i, total, callback) {

    const query = total ?
        datastore
        .createQuery('visits')
        .filter('referer_url', "=", url) :
        datastore
        .createQuery('visits')
        .filter('referer_url', "=", url)
        .filter('date', '=', yesterday);

    datastore.runQuery(query).then(results => {

        // Visits entities found.
        const visits = results[0];

        // Changing the array position to the real published URL number in the spreedsheet
        var num = i + 2;

        callback(visits.length, num, url);

    });

}
/**
 * let's get the total traffic count by URL
 * @param {Object} auth The authorization client credentials.
 * @param {Object} rows urls with the spreadsheet
 */

function setPublishedUrlID(auth, rows) {

    for (var i = 0, len = rows.length; i < len; i++) {
       
        var urlUniqueId = crypto.createHash('md5').update(rows[i]).digest("hex");
        var rowPosition = i + 2;
        var counts = [];
        counts.push(urlUniqueId);

        queryCounts = queryCounts+1;
        console.log("sheet query count: "+ queryCounts);
       
        // let's ensure that everything it's executed
        const sheets = google.sheets({
            version: 'v4',
            auth
        });
       
        sheets.spreadsheets.values.update({
            spreadsheetId: spreadsheetId,
            range: 'Rank!O' + rowPosition,
            valueInputOption: 'USER_ENTERED',
            resource: {
                values: [
                    counts
                ],
                majorDimension: 'COLUMNS',
            },

        }, (err, {
            data
        }) => {
            if (err) return console.log('The API returned an error in setPublishedUrlID: ' + err);

        });
    }

}


/**
 * Let's save the "total traffic from web beacon" in the spreedsheet
 * @param {Object} auth The authorization client credentials.
 * @param {Object} rows urls with the spreadsheet
 * @param {Boolean} total to check if is the total from today or yesterday 
 */
function setTrafficCountSheet(auth, rows, total) {

    // Let's see what total he is talking about
    var column = (total === true) ? 'P' : 'Q';

    for (var i = 0, len = rows.length; i < len; i++) {

        getUrlTrafficCount(rows[i], i, total, function(response, num, url) {

            var counts = [];
            counts.push(response);

            queryCounts = queryCounts+1;
            console.log("sheet query count: "+queryCounts);
            // let's ensure that everything it's executed
            const sheets = google.sheets({
                version: 'v4',
                auth
            });

            sheets.spreadsheets.values.update({
                spreadsheetId: spreadsheetId,
                range: 'Rank!' + column + num,
                valueInputOption: 'USER_ENTERED',

                resource: {
                    values: [
                        counts
                    ],
                    majorDimension: 'COLUMNS',
                },

            }, (err, {
                data
            }) => {
                if (err) return console.log('The API returned an error in getUrlTrafficCount: ' + err);

            });

        });

    }

}

/**
 * Let's save the "total traffic from web beacon" in the spreedsheet
 * @param {Object} auth The authorization client credentials.
 * @param {Object} rows The callback to call with the authorized client.
 */
function setAllTrafficInSheet(auth, rows) {

    // var traffic;
    for (var i = 0, len = rows.length; i < len; i++) {
        
        const urlUniqueId = crypto.createHash('md5').update(rows[i]).digest("hex");

        // Getting all Traffic
        const query = datastore
            .createQuery('visits')
            .filter('referer_url', '=', rows[i])
            .filter('date', '=', yesterday)
            .order('date');

        datastore.runQuery(query).then(results => {

            const visits = results[0];
            const dataTraffic = [urlUniqueId, yesterday, visits.length];

            const sheets = google.sheets({
                version: 'v4',
                auth
            });
             queryCounts = queryCounts+1;
            console.log("sheet query count: "+queryCounts);
   
            sheets.spreadsheets.values.append({
                spreadsheetId: spreadsheetId,
                range: 'Web Beacon Track History!A2',
                valueInputOption: 'USER_ENTERED',
                insertDataOption: 'INSERT_ROWS',
                resource: {
                    values: [dataTraffic],
                    majorDimension: 'ROWS',
                },

            }, (err, {
                data
            }) => {
                if (err) return console.log('The API returned an error in setAllTrafficInSheet: ' + err);

            });


        });

    }
}

/**
 * Ordering objects
 * @param {Object} obj The object to order 
 * @returns {Object} ordered 
 */
function sortObjKeysAlphabetically(obj) {

    var ordered = {};
    Object.keys(obj).sort().forEach(function(key) {
        ordered[key] = obj[key];
    });
    return ordered;

}

/**
 * Getting the "published URL" from the spreddsheet in the Column N:
 * @see https://docs.google.com/spreadsheets/d/1PsRwvZRLf_ZsxiLeMpxcuxlX3WC4lIJjtTvruEG5tDo/edit
 * @param {OAuth2Client} auth The authenticated Google OAuth client.
 */
function listUrls(auth) {

    const sheets = google.sheets({
        version: 'v4',
        auth
    });
    console.log("read query count: "+ readCounts);
    sheets.spreadsheets.values.get({
        spreadsheetId: spreadsheetId,
        range: 'Rank!N2:N',
        majorDimension: "COLUMNS",
    }, (err, {
        data
    }) => {
        if (err) return console.log('The API returned an error in listUrls: ' + err);
        const rows = data.values[0];

        if (rows.length) {
            readCounts = readCounts+1;
     
            // Column for unique ID by URL, a timeout for the Usage Limits of the API 
           setPublishedUrlID(auth, rows);

      
           setAllTrafficInSheet(auth, rows);
 



        } else {
            console.log('No data found.');
        }
    });

}

// Starting the Server
require('http').createServer(tracker.middleware).listen(8080)